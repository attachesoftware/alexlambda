﻿
namespace Attache.Online.MX.Desktop
{
    //[CarrierMessage(CarrierMessage.DirectionType.Upstream, true)]
    public class DocumentSent// : IEvent
    {
        public Guid DocumentId { get; set; }

        public Guid ClientId { get; set; }

        public DateTime SentAt { get; set; }

        public DocumentMetadata MetaData { get; set; }

        public byte[] ZippedDocumentPages { get; set; }
    }

    public class DocumentMetadata
    {
        public string PageSize { get; set; }
        public string Orientation { get; set; }
        public int PageCount { get; set; }
        public string Type { get; set; }
        public string Number { get; set; }
        public string Code { get; set; }
        public string RecipientIdentifier { get; set; }
        public DateTime DocumentDate { get; set; }
        public DateTime EffectiveDate { get; set; }
        public string Currency { get; set; }
        public decimal DocumentDiscount { get; set; }
        public decimal DocumentValueExDiscount { get; set; }
        public decimal Tax { get; set; }
        public decimal DocumentValueExTax { get; set; }
        public decimal DocumentBalanceOutstanding { get; set; }
        public int NumberLineItems { get; set; }
        public Service ServiceNode { get; set; }
        public From FromNode { get; set; }
        public string Subject { get; set; }
        public EmailTo EmailToNode { get; set; }
        public FaxTo FaxToNode { get; set; }
        public FileTo FileToNode { get; set; }
        public LeadingPages LeadingPagesNode { get; set; }
        public FollowingPages FollowingPagesNode { get; set; }

        public class Service
        {
            public string Domain { get; set; }
            public string IP { get; set; }
        }

        public class From
        {
            public string Address { get; set; }
            public string Name { get; set; }
            public string ReplyTo { get; set; }
        }

        public class EmailTo
        {
            public string Address { get; set; }
            public string Name { get; set; }
            public bool DeliveryNotify { get; set; }
            public bool DocumentLock { get; set; }
            public string SenderCode { get; set; }
            public string RecipientCode { get; set; }
            public string DataAttachment { get; set; }
            public string MessageFormat { get; set; }
        }

        public class FileTo
        {
            public string DeliveryProtocol { get; set; }
            public string Address { get; set; }
            public string Name { get; set; }
            public bool DocumentLock { get; set; }
            public string DocumentName { get; set; }
            public string Password { get; set; }
            public string DocumentFormat { get; set; }
            public bool DeliveryNotify { get; set; }
            public string SenderCode { get; set; }
            public string RecipientCode { get; set; }
        }

        public class FaxTo
        {
            public string Number { get; set; }
            public string Name { get; set; }
            public bool DeliveryNotify { get; set; }
            public bool DocumentLock { get; set; }
            public string SenderCode { get; set; }
            public string RecipientCode { get; set; }
            public string DataAttachment { get; set; }
            public string MessageFormat { get; set; }
        }

        public class LeadingPages
        {
            public InclusionsEntry[] Inclusions { get; set; }
        }

        public class FollowingPages
        {
            public InclusionsEntry[] Inclusions { get; set; }
        }

        public class InclusionsEntry
        {
            public int Sequence { get; set; }
            public string Path { get; set; }
        }
    }
}