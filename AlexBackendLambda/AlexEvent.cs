﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlexBackendLambda
{
    public class AlexEvent
    {
        public string EventName { get; set; } = String.Empty;
        public string Data { get; set; } = String.Empty;
    }
}
