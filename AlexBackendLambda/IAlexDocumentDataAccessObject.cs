﻿namespace AlexBackendLambda
{
    public interface IAlexDocumentDataAccessObject
    {
        bool SaveDocument(AlexDocument document);
    }
}