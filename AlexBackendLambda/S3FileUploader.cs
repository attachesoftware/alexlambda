﻿using Amazon.Lambda.Core;
using Amazon.S3;
using Amazon.S3.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlexBackendLambda
{
    public class S3FileUploader
    {


        public async Task UploadFile(string bucketName, string key, string contentType, string content)
        {
            LambdaLogger.Log("Starting file upload...");
            try
            {
                using (var client = new AmazonS3Client(Amazon.RegionEndpoint.APSoutheast2))
                {
                    LambdaLogger.Log($"{bucketName}, {key}, {contentType}, {content}");
                    var request = new PutObjectRequest
                    {
                        BucketName = bucketName,
                        Key = key,
                        ContentType = contentType,
                        ContentBody = content,
                        ServerSideEncryptionMethod = ServerSideEncryptionMethod.AES256
                    };
                    var response = await client.PutObjectAsync(request);
                }
            }
            catch (Exception ex)
            {
                LambdaLogger.Log($"Failed to upload file: {ex.Message}");
                throw;
            }
        }
    }
}
