﻿using Amazon.Lambda.Core;
using Amazon.S3;
using Amazon.S3.Model;
using Attache.Online.MX.Desktop;
using System;
using System.Collections.Generic;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace AlexBackendLambda
{
    public static class AlexDocumentS3Uploader
    {
        public static async Task UploadDocument(DocumentSent document)
        {
            using (var entryStream = new MemoryStream(document.ZippedDocumentPages))
            {
                using (var archive = new ZipArchive(entryStream, ZipArchiveMode.Read))
                {
                    foreach (var entry in archive.Entries)
                    {
                        string ext = "emf";
                        string mimeType = "image/emf";
                        if (entry.FullName.EndsWith("pdf",StringComparison.CurrentCultureIgnoreCase))
                        {
                            ext = "pdf";
                            mimeType = "application/pdf";
                        }
                        await UploadStreamAsync("alexhackfest202204", $"AlexBackend/Test/{document.DocumentId}.{ext}", mimeType, entry.Open());
                    }
                }
            }
        }

        private static async Task UploadStreamAsync(string bucketName, string key, string contentType, Stream streamContent)
        {
            var memoryStream = new MemoryStream();
            try
            {
                streamContent.CopyTo(memoryStream);
                memoryStream.Position = 0;

                using (var client = new AmazonS3Client(Amazon.RegionEndpoint.APSoutheast2))
                {
                    LambdaLogger.Log($"{bucketName}, {key}, {contentType}");
                    var request = new PutObjectRequest
                    {
                        BucketName = bucketName,
                        Key = key,
                        InputStream = memoryStream,
                        ContentType = contentType,
                        ServerSideEncryptionMethod = ServerSideEncryptionMethod.AES256
                    };
                    var response = await client.PutObjectAsync(request);
                    LambdaLogger.Log(JsonSerializer.Serialize(response));
                }
            }
            catch (Exception ex)
            {
                LambdaLogger.Log($"Failed to upload file: {ex.Message}");
                throw;
            }
            finally
            {
                memoryStream.Dispose();
            }
        }
    }
}
