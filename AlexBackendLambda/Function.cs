using Amazon.Lambda.Core;
using Amazon.Lambda.ApplicationLoadBalancerEvents;
using Amazon.S3;
using Amazon.S3.Model;
using System.Text.Json;
using Attache.Online.MX.Desktop;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.SystemTextJson.DefaultLambdaJsonSerializer))]

namespace AlexBackendLambda;

public class Function
{
    /// <summary>
    /// Lambda function handler to respond to events coming from an Application Load Balancer.
    ///
    /// Note: If "Multi value headers" is disabled on the ELB Target Group then use the Headers and QueryStringParameters properties
    /// on the ApplicationLoadBalancerRequest and ApplicationLoadBalancerResponse objects. If "Multi value headers" is enabled then
    /// use MultiValueHeaders and MultiValueQueryStringParameters properties.
    /// </summary>
    /// <param name="request"></param>
    /// <param name="context"></param>
    /// <returns></returns>
    public async Task<ApplicationLoadBalancerResponse> FunctionHandler(ApplicationLoadBalancerRequest request, ILambdaContext context)
    {
        LambdaLogger.Log("Received new event in Lambda...");
        var databaseWriter = new AlexDocumentDataAccessObject("data source=127.0.0.1,61433;initial catalog=Alex_QA;persist security info=True;user id=xelaqa;password=893LTdQ;multipleactiveresultsets=True;application name=AlexLambda;");
        var response = new ApplicationLoadBalancerResponse
        {
            StatusCode = 200,
            StatusDescription = "200 OK",
            IsBase64Encoded = false
        };
        if (!string.IsNullOrEmpty(request.Body))
        {
            var incomingRequest = JsonSerializer.Deserialize<AlexEvent>(request.Body);

            var documentEvent = GetAlexEvent(request.Body);
            if (documentEvent != null)
            {
                LambdaLogger.Log("Receied alex document");
                LambdaLogger.Log("DocumentId: " + documentEvent.DocumentId);
                LambdaLogger.Log("Metadata: " + documentEvent.MetaData);
                LambdaLogger.Log("Sent at: " + documentEvent.SentAt);
                LambdaLogger.Log("Starting upload...");
                await AlexDocumentS3Uploader.UploadDocument(documentEvent);
                LambdaLogger.Log("Upload complete.");
                LambdaLogger.Log("Starting write to DB...");
                LambdaLogger.Log(JsonSerializer.Serialize(documentEvent));
                databaseWriter.SaveDocument(new AlexDocument()
                {
                    FromAddress = documentEvent.MetaData?.FromNode?.Address,
                    ToAddress = documentEvent.MetaData?.EmailToNode?.Address,
                    FaxOrEmail = string.IsNullOrEmpty(documentEvent.MetaData?.EmailToNode?.Address?.Trim()) ? "email" : "fax",
                    FromName = documentEvent.MetaData?.FromNode.Name,
                    Subject = documentEvent.MetaData?.Subject,
                    SenderCode = documentEvent.MetaData?.EmailToNode?.SenderCode,
                    RecipientCode = documentEvent.MetaData?.EmailToNode?.RecipientCode,
                    Id = documentEvent.DocumentId,
                    CreatedOn = documentEvent.MetaData.DocumentDate,
                    NumberOfPages = documentEvent.MetaData.PageCount,
                    ReplyTo = documentEvent.MetaData.FromNode.ReplyTo,
                    DocumentType = documentEvent.MetaData.Type,
                    DocumentNumber = documentEvent.MetaData.Number,
                    RecipientName = documentEvent.MetaData.EmailToNode.Name,
                    Locked = documentEvent.MetaData.EmailToNode.DocumentLock ? "true" : "false", //What's this stored as in the DB?
                    MessageFormat = documentEvent.MetaData.EmailToNode.MessageFormat.Substring(0, 1),
                    HasDataAttachment = documentEvent.MetaData.EmailToNode.DataAttachment == null ? "Y" : "N",
                    NotifySenderOfDelivery = documentEvent.MetaData.EmailToNode.DeliveryNotify ? "True" : "False",
                    Queue = null,
                    IncludePageCount = 1,
                });
                LambdaLogger.Log("Write complete.");
            }

            // If "Multi value headers" is enabled for the ELB Target Group then use the "response.MultiValueHeaders" property instead of "response.Headers".
            response.Headers = new Dictionary<string, string>
        {
            {"Content-Type", "text/html; charset=utf-8" }
        };

            response.Body =
                    @$"
                <html>
                    <head>
                        <title>hello world</title>
                    </head>
                    <body>
                        <p>Hello World from Lambda</p>
                    </body>
                </html>
                ";
        }

        return response;
    }

    public void UploadFile(string bucketName, string key, string contentType, string content)
    {
        LambdaLogger.Log("Starting file upload...");
        try
        {
            using (var client = new AmazonS3Client(Amazon.RegionEndpoint.APSoutheast2))
            {
                LambdaLogger.Log($"{bucketName}, {key}, {contentType}, {content}");
                var request = new PutObjectRequest
                {
                    BucketName = bucketName,
                    Key = key,
                    ContentType = contentType,
                    ContentBody = content,
                    ServerSideEncryptionMethod = ServerSideEncryptionMethod.AES256
                };
                var response = client.PutObjectAsync(request);
                LambdaLogger.Log(JsonSerializer.Serialize(response));
            }
        }
        catch (Exception ex)
        {
            LambdaLogger.Log($"Failed to upload file: {ex.Message}");
            throw;
        }
    }

    protected DocumentSent? GetAlexEvent(string body)
    {
        try
        {
            var alexEvent = JsonSerializer.Deserialize<AlexEvent>(body);
            if (alexEvent != null)
            {
                if (alexEvent.EventName == "DocumentSent")
                {
                    return JsonSerializer.Deserialize<DocumentSent>(alexEvent.Data);
                }
            }
            return null;
        }
        catch (Exception ex)
        {
            LambdaLogger.Log($"Failed to deserialize alex event: {ex.Message}");
            return null;
        }
    }
}