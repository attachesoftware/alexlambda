﻿using Amazon.Lambda.Core;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlexBackendLambda
{
    public class AlexDocumentDataAccessObject : IAlexDocumentDataAccessObject
    {
        private readonly string connectionString;

        public AlexDocumentDataAccessObject(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public bool SaveDocument(AlexDocument document)
        {
            try
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    LambdaLogger.Log("About to execute DB write...");
                    var rowsUpdated = connection.Execute(@"insert into outmsg (mstatus,id,emailaddr,originatorname,addressee,replyto, recipientcode,sendercode,queue, locked,createtime,dataattachment,messageformat,documenttype, documentnumber,recipientname,timereceived,faxoremail,timestatus,deliverynotify,uniqueid, pagesincluded, noofpages, subject, filename)
                                                                       values (@Status, @MessageId, @FromAddress, @FromName, @ToAddress, @ReplyTo, @RecipientCode, @SenderCode, @Queue, @Locked, @CreatedOn, @HasDataAttachment, @MessageFormat, @DocumentType, @DocumentNumber, @RecipientName, @TimeReceived, @FaxOrEmail, @TimeOfLastStatusChange, @NotifySenderOfDelivery, @Id, @IncludePageCount, @NumberOfPages, @Subject, @FileName ) "
                                     , document);
                    LambdaLogger.Log("Executed DB write.");
                    return true;
                }
            }
            catch (Exception ex)
            {
                LambdaLogger.Log($"Failed to save alex document due to{ex.GetType().Name}: {ex.Message} {ex.StackTrace}");
                throw;
            }
            return false;
        }
    }

    public class AlexDocument
    {
        private string? replyTo;
        private string recipientName = string.Empty;
        private int numberOfPages;

        public Guid Id { get; set; }

        public string MessageId { get; set; } = string.Empty;
        public string Status { get; set; } = string.Empty;
        public string FromAddress { get; set; } = string.Empty;
        public string FromName { get; set; } = string.Empty;

        public string ToAddress { get; set; } = string.Empty;

        public string ReplyTo { get => replyTo ?? FromAddress; set => replyTo = value; }
        public string? RecipientCode { get; set; }

        public string? SenderCode { get; set; }
        public string? Queue { get; set; }
        public string? Locked { get; set; } = string.Empty;
        public DateTime CreatedOn { get; set; } = DateTime.Now;
        public string HasDataAttachment { get; set; } = string.Empty; // Valid values are "Y" or "N"
        public string MessageFormat { get; set; } = string.Empty; // "H" or "T"
        public string DocumentType { get; set; } = string.Empty; //@TODO Field may need xml tags stripping
        public string DocumentNumber { get; set; } = string.Empty;
        public string RecipientName { get => recipientName.Trim().Substring(0, recipientName.Length > 50 ? 50 : recipientName.Length); set => recipientName = value; }
        public DateTime TimeReceived { get; set; } = DateTime.Now;
        public string FaxOrEmail { get; set; } = string.Empty;
        public DateTime TimeOfLastStatusChange { get; set; } = DateTime.Now;
        public string? NotifySenderOfDelivery { get; set; } //Bizare can be null, '', 'True', 'False'
        public int IncludePageCount { get; set; }
        public int NumberOfPages { get => DocumentType == "PayAdv" ? numberOfPages - 1 : numberOfPages; set => numberOfPages = value; } // Really stores the number of attachements and should not count the xml file
        public string Subject { get; set; } = string.Empty;
        public string FileName { get; set; } = string.Empty; // Message identifer from the queue, was the name of the file the message sat in on disk
    }
}

//cols = "mstatus,id,emailaddr,originatorname,addressee,replyto"
//    vals = "'" & status & "','" & Message_ID & "','" & TSQLEscape(str_FromAddress) & "','" & TSQLEscape(Left$(Trim$(str_FromName), 50)) &"','" & TSQLEscape(delivery.ToAddress) & "'"

//    If(Len(delivery.ReplyTo) = 0) Then
//       vals = vals & ",'" & TSQLEscape(str_FromAddress) & "'"
//    Else
//        vals = vals & ",'" & TSQLEscape(delivery.ReplyTo) &"'"
//    End If

//    cols = cols & ",recipientcode,sendercode,queue"
//    vals = vals & ",'" & delivery.RecipientCode & "','" & TSQLEscape(delivery.Sendercode) &"','" & Field_Value("queue", messageBody) & "'"
//    cols = cols & ",locked,createtime"
//    vals = vals & ",'" & delivery.DocLock & "','" & Get_DateTime & "'"
//    cols = cols & ",dataattachment,messageformat,documenttype"
//    vals = vals & ",'" & delivery.DataAttachment & "','" & delivery.MessageFormat & "'"

//    If(delivery.InType = "PayAdv") Then
//      mtype = Field_Value_After_String("type", messageBody, "</type>")
//       If (Len(mtype) = 0) Then
//          mtype = Field_Value("type", messageBody)
//       End If
//    Else
//      mtype = Field_Value("type", messageBody)
//    End If

//    vals = vals & ",'" & TSQLEscape(mtype) &"'"
//    cols = cols & ",documentnumber,recipientname"
//    vals = vals & ",'" & TSQLEscape(delivery.Documentnumber) & "','" & TSQLEscape(Left$(Trim$(delivery.ToName), 50)) &"'"
//    cols = cols & ",timereceived,faxoremail"
//    vals = vals & ",'" & Get_DateTime(g_DateReceived) & "','" & delivery.InType & "'"

//    If delivery.InType = "fax" Then
//        cols = cols & ",faxservice"
//        vals = vals & ",'" & Left$(g_FaxManager.FromAddress, InStr(g_FaxManager.FromAddress, "@") -1) &"'"
//    End If

//    cols = cols & ",timestatus,deliverynotify,uniqueid"
//    vals = vals & ",'" & Get_DateTime & "','" & delivery.NotifyStr & "','" & GUID_ID & "'"

//    If (Field_Value("IncludePageCount", messageBody) <> "") Then
//        cols = cols & ",pagesincluded"
//        vals = vals & "," & Field_Value("IncludePageCount", messageBody)
//    End If

//    If (Field_Value("Orientation", messageBody) = "Landscape") Then
//      Landscape = 1
//    End If

//    If Not Store_Attachments(MailboxLocation, Landscape) Then
//       Store_Message = False
//       LogProblem "Unable to store attachments for " & Message_ID
//       Exit Function
//    End If

//    cols = cols & ",noofpages"
//    ' Don't include the XML in the document page count for PayAdv documents.
//    vals = vals & "," & IIf(delivery.InType = "PayAdv", Number_of_Attachments - 1, Number_of_Attachments)
//    cols = cols & ",subject,filename"
//    vals = vals & ",'" & TSQLEscape(Field_Value("Subject", messageBody)) & "','" & gstr_Filename & "'"

//    sql = "insert into outmsg (" & cols & ") values (" & vals & ")"

//'LogError "Store_Message (seq 1 - [" & sql & "])"  'gbc
//    Trans_conn.Execute sql, RecordsAffected, adCmdText Or adExecuteNoRecords