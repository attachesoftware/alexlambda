﻿using Shouldly;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace AlexBackendLambda.Tests
{
    public class AlexDocumentDataAccessObjectTests
    {
        [Fact]
        public void ICanInsertDBRows()
        {
            var sut = new AlexDocumentDataAccessObject("data source=127.0.0.1,61433;initial catalog=Alex_QA;persist security info=True;user id=xelaqa;password=893LTdQ;multipleactiveresultsets=True;application name=AlexLambda;");
            sut.SaveDocument(new AlexDocument
            {
                Id = Guid.NewGuid(),

                MessageId = "TESTMESSAGE",

                Status = "recieved",
                FromAddress = "test.user@theaccessgroup.com",
                FromName = "Test User",

                ToAddress = "test2.user@theaccessgroup.com",

                ReplyTo = "test3.user@theaccessgroup.com",
                RecipientCode = "RecipientCode1",

                SenderCode = "SenderCode1",
                Queue = null,
                Locked = "False",
                CreatedOn = DateTime.Now,
                HasDataAttachment = "N", // Valid values are "Y" or "N"
                MessageFormat = "H", // "H" or "T"
                DocumentType = "payadv", //@TODO Field may need xml tags stripping
                DocumentNumber = "01",
                RecipientName = "Recipient Test",
                TimeReceived = DateTime.Now,
                FaxOrEmail = "email",
                TimeOfLastStatusChange = DateTime.Now,
                NotifySenderOfDelivery = "True", //Bizare can be null, '', 'True', 'False'
                IncludePageCount = 1,
                NumberOfPages = 2, // Really stores the number of attachements and should not count the xml file
                Subject = "Test DB",
                FileName = Guid.NewGuid().ToString() // Message identifer from the queue, was the name of the file the message sat in on disk
            }).ShouldBeTrue();
        }
    }
}